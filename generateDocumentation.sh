git submodule update --init --recursive
git submodule update --remote --recursive
doxygen Doxygen
cd doc/latex
make
cd ../..
mv doc/latex/refman.pdf doc/Triangulate-Library.pdf