
git submodule update --init
git submodule update --remote

cd src/lib/obj-loader
./build.sh
cd ../../..
rm build -rf
mkdir build
cd build
cmake .. 
make -j 12
make install | egrep -v 'gmock|gtest'
