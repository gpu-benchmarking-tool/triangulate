/*__________________________________________________
 |                                                  |
 |  File: Triangulate.cpp                           |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Triangulate to add more complexity |
 |  to a 3D object.                                 |
 |_________________________________________________*/



#pragma once

#ifdef QT_APPLICATION
#include "OBJ.h"
#else
#include "lib/obj-loader/src/OBJ.h"
#endif

#include <vector>



namespace obj {


/**
 * @brief      Triangulate and subdivide an OBJ file.
 * Uses a 3D model loaded and loops through the faces ABC.
 * For each face, computes 3 new points (D, E and F).
 * D = (A + B)/2, E = (B + C)/2 and F = (C + A)/2.
 * Adds D, E and F to the model and retrieved their indices.
 * Delete the current face and create 4 faces: ADF, DBE, DEF and ECF.
 *
 * @param      model  The model to be subdivided
 */
void triangulate(OBJ &model) {

    std::vector<Face> faces;

    for (unsigned i = 0; i < model.getFaces().size(); ++i) {

        // Get the current face
        Face f = model.getFaces()[i];

        // Get A, B and C
        math::Vector3 A = model.getVertices()[f.v0];
        math::Vector3 B = model.getVertices()[f.v1];
        math::Vector3 C = model.getVertices()[f.v2];
        math::Vector2 tcA = model.getUVs()[f.tc0];
        math::Vector2 tcB = model.getUVs()[f.tc1];
        math::Vector2 tcC = model.getUVs()[f.tc2];

        // Compute D, the center of A and B
        math::Vector3 D = (A + B) / 2.;
        math::Vector2 tcD = (tcA + tcB) / 2.;

        // Compute E, the center of B and C
        math::Vector3 E = (B + C) / 2.;
        math::Vector2 tcE = (tcB + tcC) / 2.;

        // Compute F, the center of C and A
        math::Vector3 F = (C + A) / 2.;
        math::Vector2 tcF = (tcC + tcA) / 2.;

        // Add to model
        unsigned indexD = model.addVertex(D);
        unsigned indexE = model.addVertex(E);
        unsigned indexF = model.addVertex(F);
        unsigned UVIndex = 0;

        // Create the new faces
        Face f0(f.v0, f.tc0, f.n0, indexD, UVIndex, f.n0, indexF, UVIndex, f.n0);
        Face f1(indexD, UVIndex, f.n0, f.v1, f.tc1, f.n1, indexE, UVIndex, f.n0);
        Face f2(indexD, UVIndex, f.n0, indexE, UVIndex, f.n0, indexF, UVIndex, f.n0);
        Face f3(indexE, UVIndex, f.n0, f.v2, f.tc2, f.n2, indexF, UVIndex, f.n0);

        // Add the faces to the list
        faces.push_back(f0);
        faces.push_back(f1);
        faces.push_back(f2);
        faces.push_back(f3);
    }

    // Update the faces list
    model.updateFaces(faces);
}

}