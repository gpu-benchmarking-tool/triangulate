/*__________________________________________________
 |                                                  |
 |  File: Main.cpp                                  |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Used to generate the different     |
 |  cubes.                                          |
 |_________________________________________________*/



#include "Triangulate.hpp"


// Main function.
int main(int argc, char const *argv[]) {

    obj::OBJ cube("models/cube.obj");
    obj::triangulate(cube);
    cube.saveModel("models/cube1.obj");
    obj::triangulate(cube);
    cube.saveModel("models/cube2.obj");
    obj::triangulate(cube);
    cube.saveModel("models/cube3.obj");
    obj::triangulate(cube);
    cube.saveModel("models/cube4.obj");
    obj::triangulate(cube);
    cube.saveModel("models/cube5.obj");
    obj::triangulate(cube);
    cube.saveModel("models/cube6.obj");

    return 0;
}