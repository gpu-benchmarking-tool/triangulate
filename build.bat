git submodule update --init
git submodule update --remote

cd src\lib\obj-loader
call build.bat
cd ..

rmdir /s /Q bin
mkdir bin
copy build\src\libobj-loader.a bin
cd ..\..\..

rmdir /s /Q build
mkdir build
cd build
cmake .. -G "MinGW Makefiles"
cmake --build . --config Release