/*__________________________________________________
 |                                                  |
 |  File: TriangulateTests.cpp                      |
 |  Author: Nabil Sadeg                             |
 |                                                  |
 |  Description: Triangulate tests.                 |
 |_________________________________________________*/



#include "gtest/gtest.h"
#include "Triangulate.hpp"


TEST(Triangulate, UpdateFaces) {
    obj::OBJ o("models/cube.obj");

    ASSERT_TRUE(o.isLoaded());

    ASSERT_EQ(o.getVertices().size(), 8);
    ASSERT_EQ(o.getNormals().size(), 6);
    ASSERT_EQ(o.getUVs().size(), 4);
    ASSERT_EQ(o.getFaces().size(), 12);

    obj::triangulate(o);

    EXPECT_EQ(o.getVertices().size(), 44);
    EXPECT_EQ(o.getNormals().size(), 6);
    EXPECT_EQ(o.getUVs().size(), 4);
    EXPECT_EQ(o.getFaces().size(), 48);
}
